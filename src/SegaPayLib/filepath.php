<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 14/12/16
 * Time: 11:53 AM
 */

class SdkHelperFunctions {


    static function getBootStrap(){


        if(!defined('SAGEPAY_SDK_PATH')){
            define('SAGEPAY_SDK_PATH', dirname(__FILE__)); // Path to SagePay SDK
        }


        include_once SAGEPAY_SDK_PATH . '/constants.php';
        return __DIR__;
    }

}

