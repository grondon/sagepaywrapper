<?php

namespace SegayPay\Bundle\PaymentBundle\Controller;


use SegayPay\Bundle\PaymentBundle\Entity\BillindAdress;
use SegayPay\Bundle\PaymentBundle\Entity\CreditCard;
use SegayPay\Bundle\PaymentBundle\Entity\Product;
use SegayPay\Bundle\PaymentBundle\Entity\SagepayCustomer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('SegayPayBundlePaymentBundle:Default:index.html.twig');
    }

    /**
     * @Route("/notify")
     */
    public function notifyAction(Request $request)
    {
        $serverInt = $this->get("segay_pay_bundle_payment.integration.server");
        $result = $serverInt->processResponse(json_encode($request->request->all()));
        return Response::create($result['query']);
    }

    /**
     * @Route("/final")
     */
    public function finalAction()
    {
        return $this->render('SegayPayBundlePaymentBundle:Default:result.html.twig',['vtx'=>filter_input(INPUT_GET, 'vtx')]);
    }

    /**
     * @Route("/exito")
     */
    public function exitoAction()
    {
        return $this->render('SegayPayBundlePaymentBundle:Default:index.html.twig');

    }

    /**
     * @Route("/success")
     */
    public function testpaymentAction()
    {
        try {
            list($billing, $delivery, $productlist) = $this->mockData();
            $sagepayCustomer = new SagepayCustomer();
            $sagepayCustomer->setBillName("Test Billing");
            $sagepayCustomer->setBillindAdress($billing);
            $sagepayCustomer->setDeliveryAdress($delivery);

            foreach ($productlist as $product){
                $sagepayCustomer->addProducts($product);
            }

            $serverInt = $this->get("segay_pay_bundle_payment.integration.server");
            $reirect=  $serverInt->preparedRequest($sagepayCustomer, uniqid());
            return $this->redirect($reirect);
        } catch (\Exception $ex) {
            return Response::create($ex->getMessage());
        }
    }






    public function mockData()
    {

//        $reditCard = new CreditCard();
//
//
//        /***
//         *  Datos de la tarjeta de credito
//         */
//
//        $reditCard->setCardType("DELTA")->setCardNumber("4462 0000 0000 0003");
//        $reditCard->setCardHolder("Julieta Venegas");
//        $reditCard->setStartDate("1115")->setExpiryDate("1120");
//        $reditCard->setCv2("123");

        /***
         *  Datos del tarjeta habiente
         */

        $billing = new BillindAdress();
        $billing->setBillingFirstnames("Oscar de leon")->setBillingSurname("Oscar de leon")->setBillingPhone("44 (0)7933 000 000");
        $billing->setBillingAddress1("BillAddress Line 1")->setBillingAddress2("BillAddress Line 2");
        $billing->setBillingCity("BillCity")->setBillingPostCode("W1A 1BL")->setBillingCountry("GB");
        $billing->setBillingState("");
        $billing->setCustomerEmail("customer@example.com");

        $billing->setDeliveryAddress1("BillAddress Line 1")->setDeliveryAddress2("BillAddress Line 2");
        $billing->setDeliveryFirstnames("Oscar de leon")->setDeliverySurname("Oscar de leon")->setDeliveryPhone("44 (0)7933 000 000");
        $billing->setDeliveryCity("GB")->setDeliveryState("")->setDeliveryPostCode("W1A 1BL");


        /***
         *  Datos delivery
         */
        $delivery = new BillindAdress();
        $delivery->setBillingFirstnames("Oscar de leon")->setBillingSurname("Oscar de leon")->setBillingPhone("44 (0)7933 000 000");
        $delivery->setBillingAddress1("BillAddress Line 1")->setBillingAddress2("BillAddress Line 2");
        $delivery->setBillingCity("BillCity")->setBillingPostCode("W1A 1BL")->setBillingCountry("GB");
        $delivery->setBillingState("");
        $delivery->setCustomerEmail("customer@example.com");

        $delivery->setDeliveryAddress1("BillAddress Line 1")->setDeliveryAddress2("BillAddress Line 2");
        $delivery->setDeliveryFirstnames("Oscar de leon")->setDeliverySurname("Oscar de leon")->setDeliveryPhone("44 (0)7933 000 000");
        $delivery->setDeliveryCity("BillCity")->setDeliveryState("")->setDeliveryPostCode("W1A 1BL");
        $delivery->setDeliveryCountry("GB");

        /***
         * Datos del producto
         */


        $listProducts = [];

        $product = new  Product();
        $product->setDescription("Shaolin Soccer");
        $product->setProductCode("1236871");
        $product->setProductSku("DVD1SKU");
        $product->setUnitTaxAmount("0.25");
        $product->setQuantity("10");
        $product->setUnitNetAmount("9.95");
        $listProducts[]  = $product;


        $product = new  Product();
        $product->setDescription("Terminator 2: el juicio final ");
        $product->setProductCode("12359");
        $product->setProductSku("DVD1SKU");
        $product->setUnitTaxAmount("0.15");
        $product->setQuantity("10");
        $product->setUnitNetAmount("20");

        $listProducts[]  = $product;

        /***
         *  Custmer
         */


        return array($billing, $delivery, $listProducts);

    }

}
