<?php

/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 12/01/17
 * Time: 02:18 PM
 */
namespace SegayPay\Bundle\PaymentBundle\DependencyInjection;

//use Symfony\Component\HttpKernel\DependencyInjection\Extension;
//use Symfony\Component\DependencyInjection\ContainerBuilder;
//use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
//use Symfony\Component\Config\FileLocator;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;


class SegayPayBundlePaymentExtension extends Extension
{


    public function load(array $configs, ContainerBuilder $container)
    {
//        $loader = new XmlFileLoader(
//            $container,
//            new FileLocator(__DIR__.'/../Resources/config')
//        );
//        $loader->load('services.yml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('parameters.yml');
        $loader->load('services.yml');



    }
}