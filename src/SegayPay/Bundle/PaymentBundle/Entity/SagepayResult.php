<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 11/01/17
 * Time: 06:00 PM
 */

namespace SegayPay\Bundle\PaymentBundle\Entity;


class SagepayResult
{


    private $result;
    private $raw;

    /**
     * SagepayResult constructor.
     * @param $result
     */
    public function __construct($result)
    {
        $this->result = $result;

    }


    public function getVPSProtocol()
    {
        return $this->result['VPSProtocol'];
    }

    public function getTxType()
    {
        return $this->result['TxType'];
    }

    public function getVendorTxCode()
    {
        return $this->result['VendorTxCode'];
    }

    public function getVPSTxId()
    {
        return $this->result['VPSTxId'];
    }

    public function getStatus()
    {
        return $this->result['Status'];
    }

    public function getStatusDetail()
    {
        return $this->result['StatusDetail'];
    }

    public function getTxAuthNo()
    {
        return $this->result['TxAuthNo'];
    }

    public function getAVSCV2()
    {
        return $this->result['AVSCV2'];
    }

    public function getAddressResult()
    {
        return $this->result['AddressResult'];
    }

    public function getPostCodeResult()
    {
        return $this->result['PostCodeResult'];
    }

    public function getCV2Result()
    {
        return $this->result['AVSCV2'];
    }

    public function getGiftAid()
    {
        return $this->result['GiftAid'];
    }

    public function get3DSecureStatus()
    {
        return $this->result['3DSecureStatus'];
    }

    public function getCardType()
    {
        return $this->result['CardType'];
    }

    public function getLast4Digits()
    {
        return $this->result['Last4Digits'];
    }

    public function getVPSSignature()
    {
        return $this->result['VPSSignature'];
    }

    public function getDeclineCode()
    {
        return $this->result['DeclineCode'];
    }

    public function getExpiryDate()
    {
        return $this->result['ExpiryDate'];
    }

    public function getBankAuthCode()
    {
        return $this->result['BankAuthCode'];
    }


    public function getToken()
    {
        if(array_key_exists('Token', $this->result))
          return $this->result['Token'];
    }


    public function isSuccessful()
    {
        if (in_array($this->getStatus(),
            array(SAGEPAY_REMOTE_STATUS_OK, SAGEPAY_REMOTE_STATUS_AUTHENTICATED, SAGEPAY_REMOTE_STATUS_REGISTERED))) {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getRaw()
    {
        return $this->raw;
    }



    
    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }




}