<?php

/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 11/01/17
 * Time: 06:54 AM
 */

namespace SegayPay\Bundle\PaymentBundle\Entity;

class BillindAdress
{


    private $billingFirstnames;
    private $billingSurname;
    private $billingAddress2;
    private $billingAddress1;
    private $billingCity;
    private $billingPostCode;
    private $billingCountry;
    private $billingState;
    private $billingPhone;
    private $customerEmail;
    private $deliveryFirstnames;
    private $deliverySurname;
    private $deliveryAddress1;
    private $deliveryAddress2;
    private $deliveryCity;
    private $deliveryPostCode;
    private $deliveryCountry;
    private $deliveryState;
    private $deliveryPhone;

    /**
     * @return mixed
     */
    public function getBillingFirstnames()
    {
        return $this->billingFirstnames;
    }

    /**
     * @param mixed $billingFirstnames
     * @return BillindAdress
     */
    public function setBillingFirstnames($billingFirstnames)
    {
        $this->billingFirstnames = $billingFirstnames;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingSurname()
    {
        return $this->billingSurname;
    }

    /**
     * @param mixed $billingSurname
     * @return BillindAdress
     */
    public function setBillingSurname($billingSurname)
    {
        $this->billingSurname = $billingSurname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingAddress2()
    {
        return $this->billingAddress2;
    }

    /**
     * @param mixed $billingAddress2
     * @return BillindAdress
     */
    public function setBillingAddress2($billingAddress2)
    {
        $this->billingAddress2 = $billingAddress2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingCity()
    {
        return $this->billingCity;
    }

    /**
     * @param mixed $billingCity
     * @return BillindAdress
     */
    public function setBillingCity($billingCity)
    {
        $this->billingCity = $billingCity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingPostCode()
    {
        return $this->billingPostCode;
    }

    /**
     * @param mixed $billingPostCode
     * @return BillindAdress
     */
    public function setBillingPostCode($billingPostCode)
    {
        $this->billingPostCode = $billingPostCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingCountry()
    {
        return $this->billingCountry;
    }

    /**
     * @param mixed $billingCountry
     * @return BillindAdress
     */
    public function setBillingCountry($billingCountry)
    {
        $this->billingCountry = $billingCountry;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingState()
    {
        return $this->billingState;
    }

    /**
     * @param mixed $billingState
     * @return BillindAdress
     */
    public function setBillingState($billingState)
    {
        $this->billingState = $billingState;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingPhone()
    {
        return $this->billingPhone;
    }

    /**
     * @param mixed $billingPhone
     * @return BillindAdress
     */
    public function setBillingPhone($billingPhone)
    {
        $this->billingPhone = $billingPhone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @param mixed $customerEmail
     * @return BillindAdress
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliveryFirstnames()
    {
        return $this->deliveryFirstnames;
    }

    /**
     * @param mixed $deliveryFirstnames
     * @return BillindAdress
     */
    public function setDeliveryFirstnames($deliveryFirstnames)
    {
        $this->deliveryFirstnames = $deliveryFirstnames;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliverySurname()
    {
        return $this->deliverySurname;
    }

    /**
     * @param mixed $deliverySurname
     * @return BillindAdress
     */
    public function setDeliverySurname($deliverySurname)
    {
        $this->deliverySurname = $deliverySurname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliveryAddress1()
    {
        return $this->deliveryAddress1;
    }

    /**
     * @param mixed $deliveryAddress1
     * @return BillindAdress
     */
    public function setDeliveryAddress1($deliveryAddress1)
    {
        $this->deliveryAddress1 = $deliveryAddress1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliveryAddress2()
    {
        return $this->deliveryAddress2;
    }

    /**
     * @param mixed $deliveryAddress2
     * @return BillindAdress
     */
    public function setDeliveryAddress2($deliveryAddress2)
    {
        $this->deliveryAddress2 = $deliveryAddress2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliveryCity()
    {
        return $this->deliveryCity;
    }

    /**
     * @param mixed $deliveryCity
     * @return BillindAdress
     */
    public function setDeliveryCity($deliveryCity)
    {
        $this->deliveryCity = $deliveryCity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliveryPostCode()
    {
        return $this->deliveryPostCode;
    }

    /**
     * @param mixed $deliveryPostCode
     * @return BillindAdress
     */
    public function setDeliveryPostCode($deliveryPostCode)
    {
        $this->deliveryPostCode = $deliveryPostCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliveryCountry()
    {
        return $this->deliveryCountry;
    }

    /**
     * @param mixed $deliveryCountry
     * @return BillindAdress
     */
    public function setDeliveryCountry($deliveryCountry)
    {
        $this->deliveryCountry = $deliveryCountry;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliveryState()
    {
        return $this->deliveryState;
    }

    /**
     * @param mixed $deliveryState
     * @return BillindAdress
     */
    public function setDeliveryState($deliveryState)
    {
        $this->deliveryState = $deliveryState;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeliveryPhone()
    {
        return $this->deliveryPhone;
    }

    /**
     * @param mixed $deliveryPhone
     * @return BillindAdress
     */
    public function setDeliveryPhone($deliveryPhone)
    {
        $this->deliveryPhone = $deliveryPhone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBillingAddress1()
    {
        return $this->billingAddress1;
    }

    /**
     * @param mixed $billingAddress1
     * @return BillindAdress
     */
    public function setBillingAddress1($billingAddress1)
    {
        $this->billingAddress1 = $billingAddress1;
        return $this;
    }






}