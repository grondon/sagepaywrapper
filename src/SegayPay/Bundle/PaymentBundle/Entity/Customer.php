<?php
/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 11/01/17
 * Time: 08:07 AM
 */

namespace SegayPay\Bundle\PaymentBundle\Entity;


class Customer
{

    private $id;
    private $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }




}