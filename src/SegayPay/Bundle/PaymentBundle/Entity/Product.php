<?php

/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 11/01/17
 * Time: 07:05 AM
 */

namespace SegayPay\Bundle\PaymentBundle\Entity;

class Product
{

    private $description;
    private $productCode;
    private $productSku;
    private $unitTaxAmount;
    private $quantity;
    private $unitNetAmount;

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * @param mixed $productCode
     * @return Product
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductSku()
    {
        return $this->productSku;
    }

    /**
     * @param mixed $productSku
     * @return Product
     */
    public function setProductSku($productSku)
    {
        $this->productSku = $productSku;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnitTaxAmount()
    {
        return $this->unitTaxAmount;
    }

    /**
     * @param mixed $unitTaxAmount
     * @return Product
     */
    public function setUnitTaxAmount($unitTaxAmount)
    {
        $this->unitTaxAmount = $unitTaxAmount;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     * @return Product
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUnitNetAmount()
    {
        return $this->unitNetAmount;
    }

    /**
     * @param mixed $unitNetAmount
     * @return Product
     */
    public function setUnitNetAmount($unitNetAmount)
    {
        $this->unitNetAmount = $unitNetAmount;
        return $this;
    }





}