<?php



namespace SegayPay\Bundle\PaymentBundle\Entity;

/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 11/01/17
 * Time: 06:53 AM
 */


class CreditCard
{

   private $cv2;
   private $expiryDate;
   private $startDate;
   private $cardHolder;
   private $cardNumber;
   private $cardType;

    /**
     * @return mixed
     */
    public function getCv2()
    {
        return $this->cv2;
    }

    /**
     * @param mixed $cv2
     * @return CreditCard
     */
    public function setCv2($cv2)
    {
        $this->cv2 = $cv2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * @param mixed $expiryDate
     * @return CreditCard
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     * @return CreditCard
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardHolder()
    {
        return $this->cardHolder;
    }

    /**
     * @param mixed $cardHolder
     * @return CreditCard
     */
    public function setCardHolder($cardHolder)
    {
        $this->cardHolder = $cardHolder;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param mixed $cardNumber
     * @return CreditCard
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardType()
    {
        return $this->cardType;
    }

    /**
     * @param mixed $cardType
     * @return CreditCard
     */
    public function setCardType($cardType)
    {
        $this->cardType = $cardType;
        return $this;
    }







}