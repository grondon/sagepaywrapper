<?php

/**
 * Created by PhpStorm.
 * User: gerardo
 * Date: 11/01/17
 * Time: 08:03 AM
 */

namespace SegayPay\Bundle\PaymentBundle\Service;

use SagepayBasket;
use SegayPay\Bundle\PaymentBundle\Entity\BillindAdress;
use SegayPay\Bundle\PaymentBundle\Entity\CreditCard;
use SegayPay\Bundle\PaymentBundle\Entity\Customer;
use SegayPay\Bundle\PaymentBundle\Entity\Product;
use SegayPay\Bundle\PaymentBundle\Entity\SagepayCustomer;
use SegayPay\Bundle\PaymentBundle\Entity\SagepayResult;

class ServerIntegration
{




    private $config;
    private $successEndPont;
    private $failureEndPont;



    /**
     * ServerIntegration constructor.
     */
    public function __construct($vendorName,$siteFqdns,$notification_url,$success,$failure)
    {
        $this->vendorName = $vendorName;
        \SdkHelperFunctions::getBootStrap();
        $this->config = \SagepaySettings::getInstance();
        $this->config->setSiteFqdns(array(
            'live' => $siteFqdns,
            'test' => $siteFqdns
        ));
        $this->config->setVendorName($vendorName);
        $this->config->setserverNotificationUrl($notification_url);
        $this->successEndPont = $success;
        $this->failureEndPont = $failure;
    }





    public function preparedRequest(SagepayCustomer $sagepayCustomer, $orderId, $creditCardToken=null)
    {

        list($billingAdres,$delivery,$basket) = $this->getApiInfo($sagepayCustomer);

        $api = \SagepayApiFactory::create("server",  $this->config );
        $api->setBasket($basket);
        $api->addAddress($billingAdres);
        $api->addAddress($delivery);
        $api->setToken($creditCardToken);
        $response = $api->createRequest($orderId);
        if ($response['Status'] != SAGEPAY_REMOTE_STATUS_OK){
            throw new \Exception(json_encode($response));
        }
        return $response['NextURL'];

    }


    public function processResponse($result){


        $result = new SagepayResult($result);

        if (in_array($result->getStatus(),
            array(SAGEPAY_REMOTE_STATUS_OK, SAGEPAY_REMOTE_STATUS_AUTHENTICATED, SAGEPAY_REMOTE_STATUS_REGISTERED))) {

            $data = array(
                "Status" => SAGEPAY_REMOTE_STATUS_OK,
                "RedirectURL" =>  $this->config->getSiteFqdn() . $this->successEndPont . '?vtx=' . $result->getVendorTxCode(),
                "StatusDetail" => 'The transaction was successfully processed.'
            );
        }else{

            $data = array(
                "Status" => SAGEPAY_REMOTE_STATUS_OK,
                "RedirectURL" =>  $this->config->getSiteFqdn() . $this->failureEndPont . '?vtx=' . $result->getVendorTxCode(),
                "StatusDetail" => $result->getStatusDetail()
            );

        }
        return array("query"=>$this->arrayToQueryString($data,"\n"),"data"=>$result);

    }


    public function errorResponse($result)
    {

        $result = new SagepayResult($result);
        $data = array(
            "Status" => SAGEPAY_REMOTE_STATUS_ERROR,
            "RedirectURL" =>  $this->config->getSiteFqdn() . $this->failureEndPont . '?vtx=' . $result->getVendorTxCode(),
            "StatusDetail" => $result->getStatusDetail()
        );
        return $this->arrayToQueryString($data,"\n");
    }
    

    protected  function getApiInfo(SagepayCustomer $sagepayCustomer){
        $billingAdres = $this->encodeBillingAdress($sagepayCustomer->getBillindAdress(),'billing');
        $delivery = $this->encodeBillingAdress($sagepayCustomer->getDeliveryAdress(),'delivery');
        $basket = $this->encodeBasket($sagepayCustomer->getProducts());
        $basket->setDeliveryNetAmount(0);
        $basket->setDeliveryTaxAmount(0);
        $basket->setDescription($sagepayCustomer->getBillName());
        return array($billingAdres,$delivery,$basket);
    }

    protected function encodeCreditcard(CreditCard $creditCard)
    {
        return array(
            'cardType' => $creditCard->getCardType(),
            'cardNumber' => $creditCard->getCardNumber(),
            'cardHolder' => $creditCard->getCardHolder(),
            'startDate' => $creditCard->getCardHolder(),
            'expiryDate' => $creditCard->getExpiryDate(),
            'cv2' => $creditCard->getCv2(),
            'giftAid' => false
        );
    }

    protected function encodeBillingAdress(BillindAdress $adress,$type)
    {
        $details = array(
            "BillingFirstnames" => $adress->getBillingFirstnames(),
            "BillingSurname" => $adress->getBillingSurname(),
            "BillingAddress1" => $adress->getBillingAddress1(),
            "BillingAddress2" => $adress->getBillingAddress2(),
            "BillingCity" => $adress->getBillingCity(),
            "BillingPostCode" => $adress->getBillingPostCode(),
            "BillingCountry" => $adress->getBillingCountry(),
            "BillingState" => "",
            "BillingPhone" => $adress->getBillingPhone(),
            "customerEmail" => $adress->getCustomerEmail(),
            "DeliveryFirstnames" => $adress->getDeliveryFirstnames(),
            "DeliverySurname" => $adress->getDeliverySurname(),
            "DeliveryAddress1" => $adress->getDeliveryAddress1(),
            "DeliveryAddress2" => $adress->getDeliveryAddress2(),
            "DeliveryCity" => $adress->getDeliveryCity(),
            "DeliveryPostCode" => $adress->getDeliveryPostCode(),
            "DeliveryCountry" => $adress->getDeliveryCountry(),
            "DeliveryState" => "",
            "DeliveryPhone" => $adress->getDeliveryPhone());
          return $this->createCustomerDetails($details,$type);
    }

    protected function encodeCustomer(Customer $customer){
        $customerS = new \SagepayCustomer();
        $customerS->setCustomerId($customer->getId());
        return $customerS;

    }

    protected function encodeProducts(Product $product)
    {
        $item = new \SagepayItem();
        $item->setDescription($product->getDescription());
        $item->setProductCode($product->getProductCode());
        $item->setProductSku($product->getProductSku());
        $item->setUnitTaxAmount($product->getUnitTaxAmount());
        $item->setQuantity($product->getQuantity());
        $item->setUnitNetAmount($product->getUnitNetAmount());
        return $item;
    }

    protected function encodeBasket($listProducts)
    {

        $basket = new SagepayBasket();
        foreach ($listProducts as $product) {
            $basket->addItem($this->encodeProducts($product));
        }
        return $basket;


    }

    protected function createCustomerDetails($data, $type)
    {
        $customerdetails = new \SagepayCustomerDetails();
        $keys = $this->getDefaultCustomerKeys($type);
        foreach ($keys as $key => $value)
        {
            if (isset($data[$key]))
            {
                $customerdetails->$value = $data[$key];
            }
            if (isset($data[ucfirst($key)]))
            {
                $customerdetails->$value = $data[ucfirst($key)];
            }
        }
        if ($type == 'billing' && isset($data['customerEmail']))
        {
            $customerdetails->email = $data['customerEmail'];
        }
        return $customerdetails;
    }

    protected function getDefaultCustomerKeys($type)
    {
        $result = array();
        $keys = array(
            'Firstnames' => 'firstname',
            'Surname' => 'lastname',
            'Address1' => 'address1',
            'Address2' => 'address2',
            'City' => 'city',
            'PostCode' => 'postcode',
            'Country' => 'country',
            'State' => 'state',
            'Phone' => 'phone'
        );

        foreach ($keys as $key => $value)
        {
            $result[$type . $key] = $value;
        }
        return $result;
    }

    private function arrayToQueryString(array $data, $delimiter = '&', $urlencoded = false)
    {
        $queryString = '';
        $delimiterLength = strlen($delimiter);

        // Parse each value pairs and concate to query string
        foreach ($data as $name => $value)
        {
            // Apply urlencode if it is required
            if ($urlencoded)
            {
                $value = urlencode($value);
            }
            $queryString .= $name . '=' . $value . $delimiter;
        }

        // remove the last delimiter
        return substr($queryString, 0, -1 * $delimiterLength);
    }
}